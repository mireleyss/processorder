<?php
    class Order
    {
        private $orderItems;
        private Customer $customer;
        private $shippingAddress;

        public function __construct(
            $orderItems,
            Customer $customer,
            $shippingAddress
        )
        {
            $this->orderItems = $orderItems;
            $this->customer = $customer;
            $this->shippingAddress = $shippingAddress;
        }

        public function getOrderItems() {
            return $this->orderItems;
        }

        public function getCustomer(): Customer
        {
            return $this->customer;
        }

        public function getShippingAddress()
        {
            return $this->shippingAddress;
        }

        
    }

    class Customer {
        private $customer;

        public function __construct($customer)
        {
            $this->customer = $customer;
        }

        public function getName()
        {
            return $this->customer;
        }
    }

    class TotalPrice {
        private $order;

        public function __construct($order)
        {
            $this->order = $order;
        }

        public function calculateTotal() {
            // Логика расчет общей стоимости заказа
        }
    }

    class Shipping {
        private $order;

        public function __construct($order)
        {
            $this->order = $order;
        }

        public function shipOrder() {
            // Логика доставки
        }
    }

    class Payment {
        private $order;

        public function __construct($order)
        {
            $this->order = $order;
        }
        public function processPayment() {
            // Логика обработки платежа
        }
    }

    class Invoice {
        private $order;

        public function __construct($order)
        {
            $this->order = $order;
        }
        public function generateInvoice() {
            // Генерация счета-фактур
        }
    }

    class OrderProcessing {
        private Order $order;
        private TotalPrice $totalPrice;
        private Shipping $shipping;
        private Payment $payment;
        private Invoice $invoice;

        public function __construct(
            Order $order,
            TotalPrice $totalPrice,
            Shipping $shipping,
            Payment $payment,
            Invoice $invoice
        )
        {
            $this->order = $order;
            $this->totalPrice = $totalPrice;
            $this->shipping = $shipping;
            $this->payment = $payment;
            $this->invoice = $invoice;
        }

        public function processOrder() {
            $totalPrice = $this->totalPrice->calculateTotal();

            if ($payment = $this->payment->processPayment()) {
                $this->invoice->generateInvoice();
                $this->shipping->shipOrder();
            }

        }
    }
